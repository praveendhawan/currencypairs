require 'rails_helper'

RSpec.describe CurrencyPairWeekData, type: :model do
  context 'associations' do
    it { should belong_to :user_currency_pair }
  end

  context 'scopes' do
    describe 'latest' do
      let!(:latest) { create(:currency_pair_week_data, latest: true) }
      let!(:not_latest) { create(:currency_pair_week_data, latest: false) }

      it { CurrencyPairWeekData.latest.should_not include(not_latest) }
      it { CurrencyPairWeekData.latest.should include(latest) }
    end

    describe 'highest' do
      let!(:highest) { create(:currency_pair_week_data, boundry_type: 'highest') }
      let!(:not_highest) { create(:currency_pair_week_data, boundry_type: 'any_other_type' ) }

      it { CurrencyPairWeekData.highest.should_not include(not_highest) }
      it { CurrencyPairWeekData.highest.should include(highest) }
    end

    describe 'lowest' do
      let!(:lowest) { create(:currency_pair_week_data, boundry_type: 'lowest') }
      let!(:not_lowest) { create(:currency_pair_week_data, boundry_type: 'any_other_type') }

      it { CurrencyPairWeekData.lowest.should_not include(not_lowest) }
      it { CurrencyPairWeekData.lowest.should include(lowest) }
    end
  end
  context 'instance methods' do
    let!(:data) { create(:currency_pair_week_data) }
    let!(:another_data) { create(:currency_pair_week_data) }

    describe '#highest?' do
      before { another_data.update(boundry_type: 'any_other_type') }

      it { expect(data.highest?).to be_truthy }
      it { expect(another_data.highest?).to be_falsey }
    end

    describe '#lowest?' do
      before { another_data.update(boundry_type: 'lowest') }

      it { expect(data.lowest?).to be_falsey }
      it { expect(another_data.lowest?).to be_truthy }
    end

    describe '#special?' do
      context 'data is latest' do
        before do
          data.update(boundry_type: nil, latest: true)
          another_data.update(boundry_type: nil, latest: false)
        end

        it { expect(data.special?).to be_truthy }
        it { expect(another_data.special?).to be_falsey }
      end

      context 'data at boundry' do
        before do
          data.update(boundry_type: 'any_boundry_type', latest: false)
          another_data.update(boundry_type: nil, latest: false)
        end

        it { expect(data.special?).to be_truthy }
        it { expect(another_data.special?).to be_falsey }
      end
    end

    describe '#at_boundry?' do
      before do
        data.update(boundry_type: 'any_boundry_type', latest: false)
        another_data.update(boundry_type: nil, latest: false)
      end

      it { expect(data.at_boundry?).to be_truthy }
      it { expect(another_data.at_boundry?).to be_falsey }
    end
  end
end
