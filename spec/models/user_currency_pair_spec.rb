require 'rails_helper'

RSpec.describe UserCurrencyPair, type: :model do
  context 'associations' do
    it { should belong_to :user }
    it { should have_many :currency_pair_week_datas }
  end

  context 'validations' do
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:base_currency) }
    it { should validate_presence_of(:target_currency) }
    it { should validate_presence_of(:amount) }
    it { should validate_presence_of(:weeks) }
  end
end
