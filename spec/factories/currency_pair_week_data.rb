FactoryBot.define do
  factory :currency_pair_week_data, class: 'CurrencyPairWeekData' do
    association :user_currency_pair, factory: :user_currency_pair
    year { 2020 }
    week_number { 1 }
    week_start_date { Date.parse('30-12-2019') }
    rate { 78.45 }
    amount_valuation { 1000 }
    latest { false }
    boundry_type { 'highest' }
  end
end
