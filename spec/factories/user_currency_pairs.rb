FactoryBot.define do
  factory :user_currency_pair, class: 'UserCurrencyPair' do
    association :user, factory: :user
    base_currency { 'EUR' }
    target_currency { 'INR' }
    amount { 20 }
    weeks { 1 }
  end
end
