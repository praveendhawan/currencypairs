require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#get_profit_loss" do
    subject { helper.get_profit_loss(20, 16) }

    it { is_expected.to eql(-20.0) }
  end

  describe "#weekly_data_css_class" do
    let!(:week_data) { create(:currency_pair_week_data) }
    subject { helper.weekly_data_css_class(week_data) }

    context 'when highest' do
      before { week_data.update(boundry_type: 'highest') }

      it { is_expected.to eql('table-success') }
    end

    context 'when lowest' do
      before { week_data.update(boundry_type: 'lowest') }

      it { is_expected.to eql('table-warning') }
    end

    context 'when other' do
      before { week_data.update(boundry_type: 'any_other_type') }

      it { is_expected.to eql('') }
    end
  end
end
