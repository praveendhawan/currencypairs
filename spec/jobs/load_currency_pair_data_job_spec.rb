require 'rails_helper'

RSpec.describe LoadCurrencyPairDataJob, type: :job do
  describe '#perform_later' do
    let!(:user_currency_pair) { create(:user_currency_pair) }

    it 'enqueues job' do
      ActiveJob::Base.queue_adapter = :test

      expect {
        LoadCurrencyPairDataJob.perform_later(user_currency_pair)
      }.to have_enqueued_job
    end
  end
end
