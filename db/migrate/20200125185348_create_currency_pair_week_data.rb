class CreateCurrencyPairWeekData < ActiveRecord::Migration[6.0]
  def change
    create_table :currency_pair_week_data do |t|
      t.references :user_currency_pair, null: false, foreign_key: true
      t.integer :year
      t.integer :week_number
      t.date :week_start_date
      t.float :rate
      t.float :amount_valuation

      t.timestamps
    end
  end
end
