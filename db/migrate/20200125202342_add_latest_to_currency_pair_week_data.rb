class AddLatestToCurrencyPairWeekData < ActiveRecord::Migration[6.0]
  def change
    add_column :currency_pair_week_data, :latest, :boolean
  end
end
