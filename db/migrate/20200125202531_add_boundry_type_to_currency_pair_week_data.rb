class AddBoundryTypeToCurrencyPairWeekData < ActiveRecord::Migration[6.0]
  def change
    add_column :currency_pair_week_data, :boundry_type, :string
  end
end
