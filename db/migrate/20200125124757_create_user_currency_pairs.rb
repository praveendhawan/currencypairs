class CreateUserCurrencyPairs < ActiveRecord::Migration[6.0]
  def change
    create_table :user_currency_pairs do |t|
      t.references :user
      t.string :base_currency
      t.string :target_currency
      t.float :amount
      t.integer :weeks

      t.timestamps
    end
  end
end
