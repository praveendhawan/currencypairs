class HomeController < ApplicationController
  def index
    redirect_to user_currency_pairs_path
  end
end
