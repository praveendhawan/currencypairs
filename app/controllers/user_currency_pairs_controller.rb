class UserCurrencyPairsController < ApplicationController
  before_action :load_user_currency_pair, only: %i[edit update destroy show]

  def new
    @user_currency_pair = UserCurrencyPair.new
  end

  def create
    @user_currency_pair = UserCurrencyPair.new(user_currency_pairs_params)
    if @user_currency_pair.save!
      LoadCurrencyPairDataJob.perform_later @user_currency_pair
      redirect_to user_currency_pairs_path, notice: 'successfully created'
    else
      render partial: :form, alert: 'Could not create'
    end
  end

  def index
    @user_currency_pairs = current_user.user_currency_pairs
  end

  def show
    @weekly_data =
      @user_currency_pair
      .currency_pair_week_datas.where(latest: nil)

    load_latest_rate
  end

  def edit; end

  def update
    if @user_currency_pair.update!(user_currency_pairs_params)
      LoadCurrencyPairDataJob.perform_later @user_currency_pair
      redirect_to user_currency_pairs_path, notice: 'successfully updated'
    else
      render :form, alert: 'Could not update'
    end
  end

  def destroy
    if @user_currency_pair.destroy
      redirect_to user_currency_pairs_path, notice: 'successfully deleted'
    else
      render :form, alert: 'Could not delete'
    end
  end

  private

  def user_currency_pairs_params
    @user_currency_pairs_params ||=
      params.require(:user_currency_pair)
            .permit(%i[user_id base_currency amount target_currency weeks])
  end

  def load_user_currency_pair
    @user_currency_pair = current_user.user_currency_pairs.find(params[:id])
    return if @user_currency_pair

    redirect_to user_currency_pairs_path, notice: 'Not Found'
  end

  def load_latest_rate
    latest_data = @user_currency_pair
                  .currency_pair_week_datas.find_by(week_start_date: Date.today)
    @latest_rate = latest_data&.rate
    return if @latest_rate

    response = FixerIoServices::ApiClient
               .new(@user_currency_pair.target_currency, false, Date.today)
               .call
    @latest_rate = JSON.parse(response.body)
                   .dig('rates', @user_currency_pair.target_currency).to_f
  end
end
