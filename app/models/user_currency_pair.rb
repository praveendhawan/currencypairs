class UserCurrencyPair < ApplicationRecord
  belongs_to :user
  has_many :currency_pair_week_datas

  validates :user, :base_currency, :target_currency, :amount, :weeks, presence: true
end
