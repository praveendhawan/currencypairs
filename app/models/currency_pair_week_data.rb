class CurrencyPairWeekData < ApplicationRecord
  belongs_to :user_currency_pair

  scope :latest, -> { where(latest: true) }
  scope :highest, -> { where(boundry_type: 'highest') }
  scope :lowest, -> { where(boundry_type: 'lowest') }

  def highest?
    at_boundry? && boundry_type == 'highest'
  end

  def lowest?
    at_boundry? && boundry_type == 'lowest'
  end

  def special?
    latest? || at_boundry?
  end

  def at_boundry?
    boundry_type.present?
  end
end
