module ApplicationHelper
  def get_profit_loss(base_rate, rate)
    margin = rate - base_rate
    margin_percent = (margin.to_f / base_rate) * 100
    margin_percent.round(2)
  end

  def weekly_data_css_class(week_data)
    if week_data.highest?
      'table-success'
    elsif week_data.lowest?
      'table-warning'
    else
      ''
    end
  end
end
