class LoadCurrencyPairDataJob < ApplicationJob
  queue_as :default

  def perform(user_currency_pair)
    CurrencyPairsDataServices::LoadCurrencyPairData
      .new(user_currency_pair).call
  end
end
