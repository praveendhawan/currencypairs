require 'faraday'
require 'json'

module FixerIoServices
  class ApiClient
    BASE_CURRENCY = 'EUR'.freeze
    BASE_URL = 'http://data.fixer.io/api/'.freeze
    ACCESS_KEY = ENV['FIXER_IO_API_KEY']
    DATE_FORMAT = '%F'.freeze

    def initialize(target_currency, is_latest_request = false, date = Date.today)
      @is_latest_price_request = is_latest_request
      @date = date
      @target_currency = target_currency
    end

    def call
      request = Faraday.new(url: url) do |faraday|
        faraday.headers['Content-Type'] = 'application/json'
      end

      request.get
    end

    private

    attr_reader :is_latest_price_request, :date, :target_currency

    def url
      "#{BASE_URL}#{api_end_point}?access_key=#{ACCESS_KEY}" \
        "&base=#{BASE_CURRENCY}&symbols=#{target_currency}"
    end

    def api_end_point
      is_latest_price_request ? 'latest' : date.strftime(DATE_FORMAT)
    end
  end
end
