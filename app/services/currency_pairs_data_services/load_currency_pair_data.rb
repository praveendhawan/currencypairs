module CurrencyPairsDataServices
  class LoadCurrencyPairData
    def initialize(currency_pair)
      @currency_pair = currency_pair
      @today = Date.today
      @end_date = currency_pair.created_at.to_date.beginning_of_week
      @start_date = end_date - currency_pair.weeks.weeks
    end

    def call
      start_date.step(end_date, 7) do |date|
        next if weekly_data_exists?(date)

        response = FixerIoServices::ApiClient
                   .new(currency_pair.target_currency, false, date)
                   .call
        create_data_objects(JSON.parse(response.body), date)
      end

      response = FixerIoServices::ApiClient
                 .new(currency_pair.target_currency, true)
                 .call
      create_data_objects(JSON.parse(response.body), today)

      TagCurrencyPairWeekData.new(currency_pair).call
    end

    private

    attr_reader :currency_pair, :today, :end_date, :start_date

    def create_data_objects(response_body, date)
      rate = response_body.dig('rates', currency_pair.target_currency).to_f

      currency_pair.currency_pair_week_datas.create!(
        year: date.year,
        week_number: date.cweek,
        week_start_date: date,
        rate: rate,
        amount_valuation: currency_pair.amount * rate
      )
    end

    def weekly_data_exists?(date)
      currency_pair.currency_pair_week_datas.any? do |week_data|
        week_data.week_start_date == date
      end
    end
  end
end
