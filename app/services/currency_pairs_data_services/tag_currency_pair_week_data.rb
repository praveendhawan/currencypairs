module CurrencyPairsDataServices
  class TagCurrencyPairWeekData
    def initialize(currency_pair)
      @currency_pair = currency_pair
      @weekly_data = currency_pair.currency_pair_week_datas
    end

    def call
      add_latest_tag
      add_boundry_tag
    end

    private

    attr_reader :currency_pair, :weekly_data

    def add_latest_tag
      @weekly_data.latest.update_all(latest: false)
      @weekly_data.where(week_start_date: Date.today).update(latest: true)
    end

    def add_boundry_tag
      @weekly_data.where.not(boundry_type: nil).update_all(boundry_type: nil)
      sorted_weekly_data = @weekly_data.order(:rate)
      sorted_weekly_data.first.update(boundry_type: 'lowest')
      sorted_weekly_data.last.update(boundry_type: 'highest')
    end
  end
end
